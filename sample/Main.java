package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



public class Main extends Application {


    @FXML
    private static Stage primStage = new Stage();
    @Override
    public void start(Stage primSt) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("uploadImageWindow.fxml"));
        primStage = primSt;
        primStage.setTitle("FishEye Filter");
        primStage.setScene(new Scene(root));
        primStage.show();
    }
    public static Stage getPrimStage() {
        return primStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
