package sample;

import javafx.scene.image.Image;

public class imageClass {
    private Image image;

    public imageClass() {    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
