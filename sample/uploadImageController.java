package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;

public class uploadImageController {
    private imageClass imageImpl = new imageClass();

    public void uploadImage(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPG", "*.jpg"));
        File file = fileChooser.showOpenDialog(sample.Main.getPrimStage());
        Image image = new Image(file.toURI().toURL().toExternalForm());
        this.imageImpl.setImage(image);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("showImageWindow.fxml"));

        loader.load();
        showImageController imageController = loader.getController();
        imageController.setImageImpl(this.imageImpl);
        Parent root = loader.getRoot();
        sample.Main.getPrimStage().setScene(new Scene(root));
        imageController.imageView();
    }
}
