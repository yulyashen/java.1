package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import java.lang.Object.*;

public class showImageController {

    private imageClass currentImageImpl = new imageClass();
    private imageClass originalImageImpl = new imageClass();
    @FXML
    private ImageView imageViewImpl;
    @FXML
    private Slider fishEyeSizeSlider;
    private DitherMain ditherMainImpl;

    public void setImageImpl(imageClass img){
        this.currentImageImpl.setImage(img.getImage());
        this.originalImageImpl.setImage(img.getImage());
    }

    public void imageView() throws IOException {
        this.imageViewImpl.setImage(this.currentImageImpl.getImage());
    }

    public void applyFilter(ActionEvent actionEvent){
        try {
            BufferedImage src_img = SwingFXUtils.fromFXImage(this.originalImageImpl.getImage(), null);
            this.ditherMainImpl = new DitherMain();
            this.ditherMainImpl.setK(this.fishEyeSizeSlider.getValue());
            this.ditherMainImpl.makeImage(src_img);
            BufferedImage destin_img = this.ditherMainImpl.getEndImage();
            this.currentImageImpl.setImage(SwingFXUtils.toFXImage(destin_img, null));
            imageView();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void downloadImage(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        File destination = fileChooser.showSaveDialog(sample.Main.getPrimStage());
        if (destination != null) {
            ImageIO.write(SwingFXUtils.fromFXImage(this.currentImageImpl.getImage(),
                    null), "png", destination);
        }
    }

    public void chooseOtherImage(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("uploadImageWindow.fxml"));

        loader.load();
        uploadImageController imageController = loader.getController();
        imageController.uploadImage(new ActionEvent());
    }


}
